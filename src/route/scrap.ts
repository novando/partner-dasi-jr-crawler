import express from 'express'
import scrapIwkbu from '@service/scrapper/iwkbu'
import scrapIncome from '@service/scrapper/income'
import scrapIwkbuKantor from '@service/scrapper/iwkbu-by-kantor-codes'

const router = express.Router()

router.post('/iwkbu', (req, res) => {
  scrapIwkbu.scraper(req.body)
  res.json({message: 'SCRAP_IWKBU', data: null}).status(200)
})
router.post('/income', (req, res) => {
  // scrapIncome.scraper({user: 'mira.hayuningtyas', pass: '@Jateng100', vehicles: [{id: 1, nopol: 'H-1458-AM'}, {id: 2, nopol: 'K-1717-CP'}, {id: 3, nopol: 'K-7001-OF'}]})
  scrapIncome.scraper(req.body)
  console.log(req.body)
  res.json({message: 'SCRAP_INCOME', data: null}).status(200)
})
router.post('/iwkbu/kantor-code', (req, res) => {
  scrapIwkbuKantor.scraper(req.body)
  console.log(req.body)
  res.json({message: 'SCRAPING_IWKBU', data: null}).status(200)
})

export default router