import puppeteer from 'puppeteer'

interface HtmlDataType {
  html: string,
}

export default {
  async scraper (req:HtmlDataType) {
    const browser = await puppeteer.launch({headless:true})
    try {
      const page = await browser.newPage()
      await page.setUserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36 WAIT_UNTIL=load")
      await page.setContent(req.html)
      await page.waitForNetworkIdle()
      const res = await page.pdf({
        format: 'A4',
        printBackground: true,
        margin: {
          top: 16,
          left: 16,
          bottom: 16,
          right: 16,
        }
      })
      await browser.close()
      return res
    } catch (err) {
      console.error(err)
      await browser.close()
    }
  }
}