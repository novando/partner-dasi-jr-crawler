import puppeteer, { Page } from 'puppeteer'
import libFetch from '@src/library/fetch'

interface dasiReqType {
  user: string,
  pass: string,
  idPos: string[]
}
interface armadaDataType {
  nopol?: string,
  izin?: string,
  kodeTarif?: string,
  seat?: number,
  maxSeat?: number,
  tarif?: number,
}
interface linesReqType {
  idPo?: string,
  name?: string,
  kat?: number,
  kantor?: string,
  armada?: armadaDataType[],
}

const searchArmada = async (page:Page, idPos:string[]) => {
  await page.waitForSelector('#ctl00_ContentPlaceHolder1_txt_PO')
  await page.type('#ctl00_ContentPlaceHolder1_txt_PO', idPos[0])
  await page.waitForSelector('.autocomplete > div')

  // Scrap PO Data
  const payload:linesReqType = {}
  const poData = await page.$eval('.autocomplete > div:first-child', el => el.textContent)
  console.log(poData)
  const poDataSplitted = (poData as string).split('-Kat')
  const poGeneral = poDataSplitted[0].split(" - ")
  const poStatus = poDataSplitted[1].split(" - ")
  const poStatusSplit = poStatus[1].split('[')[1]
  let poName = ''
  for (let i = 1; i < poGeneral.length; i++) {
    poName += poGeneral[i]
  }
  payload.idPo = poGeneral[0]
  payload.name = poName.trim()
  payload.kantor = poStatusSplit.substring(0, poStatusSplit.length - 1)
  payload.kat = parseInt(poStatus[0].trim())

  await page.click('.autocomplete > div:first-child')
  await page.click('#ctl00_ContentPlaceHolder1_btnTampilkan')
  try {
    await page.waitForSelector('#ctl00_ContentPlaceHolder1_gridArmada > tbody > tr', {timeout:5000})
  } catch {
    idPos.shift()
    if (idPos.length < 1) return
    await page.reload()
    await searchArmada(page, idPos)
  }
  const armadaTable = await page.$eval('#ctl00_ContentPlaceHolder1_gridArmada > tbody', (el) => el.innerHTML)
  const trRegex = /<tr[^>]*>([\s\S]*?)<\/tr>/g
  const tdRegex = /<td[^>]*>([^<input]*?)<\/td>/g
  const trContents:armadaDataType[] = []
  let trMatch
  while ((trMatch = trRegex.exec(armadaTable)) !== null) {
    const armadaData:armadaDataType = {}
    const trText = trMatch[1]
    const tdContents = []
    let tdMatch
    while ((tdMatch = tdRegex.exec(trText)) !== null) {
      const tdText = tdMatch[1].trim()
      tdContents.push(tdText)
    }
    const regex = /^[A-Za-z]{1,2}-\d{1,4}-[A-Za-z]{1,3}$/
    if (!regex.test(tdContents[1])) continue
    const seatRegex = /.*\/([\s\S]*?)\]/
    const maxSeatRegex = /.*\[([\s\S]*?)\//
    const remarkSplitted = tdContents[7].split('-')
    const seatString = seatRegex.exec(remarkSplitted[2]) as RegExpExecArray
    const maxSeatString = maxSeatRegex.exec(remarkSplitted[2]) as RegExpExecArray
    armadaData.nopol = tdContents[1]
    armadaData.kodeTarif = remarkSplitted[0]
    armadaData.tarif = parseInt(remarkSplitted[1].trim())
    armadaData.izin = remarkSplitted[2].split(' [')[0].trim()
    armadaData.maxSeat = parseInt(maxSeatString[1].trim())
    armadaData.seat = parseInt(seatString[1].trim())
    trContents.push(armadaData)
  }
  payload.armada = trContents
  console.log(payload)
  await libFetch.postData('http://localhost:3005/webhook/sync-dasi', payload)
  idPos.shift()
  if (idPos.length < 1) return
  await page.reload()
  await searchArmada(page, idPos)
}

export default {
  async scraper (poId:dasiReqType) {
    const browser = await puppeteer.launch({headless:'new'})
    try {
      const page = await browser.newPage()
      await page.goto('https://dasi.jasaraharja.co.id/', {timeout: 60*60*1000})
      await page.setViewport({width: 1080, height: 1024})
      await page.waitForNetworkIdle({timeout: (60*60*1000)})
      const captchaSelector = await page.waitForSelector('input[name="hdCa"]')
      const captcha = await captchaSelector?.evaluate(el => el.value)
      const menuTeknikSelector = '#listContainer > ul > li:nth-child(3)'
      const menuIwSelector = `${menuTeknikSelector} > ul > li:nth-child(2)`
      const menuIwkbuSelector = `${menuIwSelector} > ul > li:nth-child(2)`
      const operasionalSelector = `${menuIwkbuSelector} > ul > li:nth-child(3)`
      const penerimaanIwkbuSelector = `${operasionalSelector} > ul > li:nth-child(1)`
      const iwkbuBatchSelector = 'tr[id="ctl00_ContentPlaceHolder1_btn_Tambah_DXI0i1_"]'
      await page.type('input[name="txtUSRID"]', poId.user)
      await page.type('input[name="txtPWD"]', poId.pass)
      await page.type('input[name="txtCaptcha"]', captcha as string)
      await page.click('input[name="chkView"]')
      await page.click('input[id="btnPro"]')
      await page.waitForNetworkIdle({timeout: (60*60*1000)})
      await page.waitForSelector(menuTeknikSelector)
      await page.click(menuTeknikSelector)
      await page.waitForSelector(menuIwSelector)
      await page.hover(menuIwSelector)
      await page.waitForSelector(menuIwkbuSelector)
      await page.hover(menuIwkbuSelector)
      await page.waitForSelector(operasionalSelector)
      await page.hover(operasionalSelector)
      await page.waitForSelector(penerimaanIwkbuSelector)
      await page.click(penerimaanIwkbuSelector)
      await page.waitForSelector(iwkbuBatchSelector)
      try {
        await page.waitForFunction("aspxMIClick(event, 'ctl00_ContentPlaceHolder1_btn_Tambah', '0i1')", {timeout: 1000})
      } catch (err) {
        console.error('Error called purposely')
      }    
      const iframeElement = await page.waitForSelector('.tcontent > iframe')
      const formUrl = await iframeElement?.evaluate(el => el.src)
      console.log(formUrl)
      await page.goto(formUrl as string)
      // iframeElement.contentFrame()
      await searchArmada(page, poId.idPos)
      await browser.close()
    } catch (err:any) {
      console.log('catch: ', err.message)
      await browser.close()
    }
  }
}