
import puppeteer, { Page } from 'puppeteer'
import libFetch from '@src/library/fetch'
import dayjs, { Dayjs } from 'dayjs'
import customParseFormat from 'dayjs/plugin/customParseFormat'
import utc from 'dayjs/plugin/utc'

type TypeReqType = 'iwkbu-current'|'iwkbu-before'|'last-3-days'
interface DasiReqType {
  username: string,
  password: string,
  reqType: TypeReqType,
  date?: string,
  codes: string[],
}
interface IncomeDataType {
  nopol: string,
  kantor: string,
  receipt: string,
  recorded: string,
  iwkbu: string,
  swdkllj: string,
  price: number,
}

dayjs.extend(customParseFormat)
dayjs.extend(utc)

const yearPast = dayjs().year() - 1
const yearNow = dayjs().year()
const iwkbuApi = process.env.API_IWKBU_BY_KANTOR_CODES_WEBHOOK as string
const vehicleDasiData:IncomeDataType[] = []

const scrapSwdkllj = async (page:Page, vehicleData:IncomeDataType[], reqType:TypeReqType) => {
  const year = reqType === 'iwkbu-before' ? dayjs().year() - 1 : dayjs().year()
  try {
    await page.waitForNetworkIdle({timeout: (60*60*1000)})
    await page.evaluate(() => (document.querySelector('input[name="ctl00$MainContent$txtCariNoPolisi"]') as HTMLInputElement).value = '')
    await page.type('input[name="ctl00$MainContent$txtCariNoPolisi"]', vehicleData[0].nopol)
    await page.click('#ctl00_MainContent_btn_cari')
    await page.waitForNetworkIdle({timeout: (60*60*1000)})
    await page.waitForSelector('#ctl00_MainContent_gridTransaksi > tbody > tr:nth-child(2)')
    const entrySwdkllj = await page.$eval('#ctl00_MainContent_gridTransaksi > tbody', (el) => el.innerHTML)
    const trRegex = /<tr[^>]*>([\s\S]*?)<\/tr>/g
    const tdRegex = /<td[^>]*>([\s\S]*?)<\/td>/g
    let trMatch
    while ((trMatch = trRegex.exec(entrySwdkllj)) !== null) {
      const tdContents = []
      let tdMatch
      while ((tdMatch = tdRegex.exec(trMatch[1])) !== null) {
        const tdText = tdMatch[1].trim()
        if (tdText) tdContents.push(tdText)
      }
      if (tdContents.length < 1) continue
      const takwimYear = tdContents[8].replaceAll(/\D+/g, '')
      if (takwimYear.length < 1) continue
      if (parseInt(takwimYear) < year) break
      if (parseInt(takwimYear) > year) continue
      vehicleData[0].swdkllj = dayjs(tdContents[10], 'DD/MM/YYYY').local().format()
    }
  } catch (err) {
    console.log(`Last year SWDKLLJ for ${vehicleData[0].nopol} not found`)
  }
  console.log(vehicleData[0])
  try {
    const res = await libFetch.postData(`${iwkbuApi}/${reqType}`, vehicleData[0])
    console.log(res.message)
  } catch (err:any) {
    console.log(err.message)
  }
  vehicleData.shift()
  if (vehicleData.length < 1) return
  await scrapSwdkllj(page, vehicleData, reqType)
}

const scrapIwkbu = async (page:Page, officeCode:string, currentDate:Dayjs, type:TypeReqType, attempt = 0):Promise<IncomeDataType[]> => {
  let startDate = currentDate.subtract(3, 'days').format('DD/MM/YYYY')
  let endDate = currentDate.format('DD/MM/YYYY')
  if (type === 'iwkbu-before') {
    startDate = currentDate.subtract(1, 'year').startOf('month').format('DD/MM/YYYY')
    endDate = currentDate.subtract(1, 'year').endOf('month').format('DD/MM/YYYY')
  }
  if (type === 'iwkbu-current') {
    startDate = currentDate.startOf('month').format('DD/MM/YYYY')
    endDate = currentDate.endOf('month').format('DD/MM/YYYY')
  }
  const vehicleIncomeData:IncomeDataType[] = []
  try {
    const menuTeknikSelector = '#listContainer > ul > li:nth-child(3)'
    const menuIwSelector = `${menuTeknikSelector} > ul > li:nth-child(2)`
    const menuIwkbuSelector = `${menuIwSelector} > ul > li:nth-child(2)`
    const operasionalSelector = `${menuIwkbuSelector} > ul > li:nth-child(3)`
    const penerimaanIwkbuSelector = `${operasionalSelector} > ul > li:nth-child(1)`
    await page.waitForNetworkIdle({timeout: (60*60*1000)})
    await page.waitForSelector(menuTeknikSelector)
    await page.click(menuTeknikSelector)
    await page.waitForSelector(menuIwSelector)
    await page.hover(menuIwSelector)
    await page.waitForSelector(menuIwkbuSelector)
    await page.hover(menuIwkbuSelector)
    await page.waitForSelector(operasionalSelector)
    await page.hover(operasionalSelector)
    await page.waitForSelector(penerimaanIwkbuSelector)
    await page.click(penerimaanIwkbuSelector)
    await page.waitForNetworkIdle({timeout: (60*60*1000)})
    await page.waitForSelector('input[name="ctl00$ContentPlaceHolder1$txt_Tgl_Transaksi_Awal"]')
    await page.evaluate(() => (document.querySelector('input[name="ctl00$ContentPlaceHolder1$txt_Tgl_Transaksi_Awal"]') as HTMLInputElement).value = '')
    await page.evaluate(() => (document.querySelector('input[name="ctl00$ContentPlaceHolder1$txt_Tgl_Transaksi_Akhir"]') as HTMLInputElement).value = '')
    await page.evaluate(() => (document.querySelector('input[name="ctl00$ContentPlaceHolder1$txtKodeKantor"]') as HTMLInputElement).value = '')
    await page.evaluate(() => (document.querySelector('input[name="ctl00$ContentPlaceHolder1$txt_Nopol1"]') as HTMLInputElement).value = '')
    await page.evaluate(() => (document.querySelector('input[name="ctl00$ContentPlaceHolder1$txt_Nopol2"]') as HTMLInputElement).value = '')
    await page.evaluate(() => (document.querySelector('input[name="ctl00$ContentPlaceHolder1$txt_Nopol3"]') as HTMLInputElement).value = '')
    await page.type('input[name="ctl00$ContentPlaceHolder1$txt_Tgl_Transaksi_Awal"]', startDate)
    await page.type('input[name="ctl00$ContentPlaceHolder1$txt_Tgl_Transaksi_Akhir"]', endDate)
    await page.type('input[name="ctl00$ContentPlaceHolder1$txtKodeKantor"]', officeCode)
    await page.waitForNetworkIdle({timeout: (60*60*1000)})
    await page.waitForSelector('.autocomplete > div')
    await page.click('.autocomplete > div:first-child')
    await page.click('#ctl00_ContentPlaceHolder1_btn_Cari')
    await page.waitForNetworkIdle({timeout: (60*60*1000)})
    await page.waitForSelector('#ctl00_ContentPlaceHolder1_gridiwkbu > tbody')
    const entryIwkbu = await page.$eval('#ctl00_ContentPlaceHolder1_gridiwkbu > tbody', (el) => el.innerHTML)
    const trRegex = /<tr[^>]*>([\s\S]*?)<\/tr>/g
    const labelRegex = /<label[^>]*>([\s\S]*?)<\/label>/g
    let trMatch
    while ((trMatch = trRegex.exec(entryIwkbu)) !== null) {
      const labelContents = []
      let labelMatch
      while ((labelMatch = labelRegex.exec(trMatch[1])) !== null) {
        labelContents.push(labelMatch[1])
      }
      const iwkbuData = {
        iwkbu: labelContents[6].trim() === '' ? dayjs('01/01/1970 00:00:01', 'DD/MM/YYYY HH:mm:ss').utc(true).format() : dayjs(labelContents[6].trim(), 'DD/MM/YYYY').local().format(),
        recorded: labelContents[2].trim() === '' ? dayjs('01/01/1970 00:00:01', 'DD/MM/YYYY HH:mm:ss').utc(true).format() : dayjs(labelContents[2].trim(), 'DD/MM/YYYY').local().format(),
        receipt: labelContents[3].trim(),
        price: parseInt(labelContents[8].trim().replaceAll(/\D+/g, '')),
        nopol: labelContents[1].trim(),
        kantor: officeCode,
        swdkllj: dayjs('01/01/1970 00:00:01', 'DD/MM/YYYY HH:mm:ss').utc(true).format(),
      }
      vehicleIncomeData.push(iwkbuData)
      vehicleDasiData.push(iwkbuData)
    }
    console.log(`\x1b[32m Scrapped IWKBU for Office ${officeCode}\x1b[0m`)
    return vehicleIncomeData
  } catch (err:any) {
    const retry = attempt + 1
    if (retry <= 3) {
      console.log(`\x1b[31m Failed to scrap IWKBU for Office ${officeCode}, retry attempt ${retry}\x1b[0m`)
      return scrapIwkbu(page, officeCode, currentDate, type, retry)
    } else {
      return vehicleIncomeData
    }
  }
}
const sendData = async (vehicle: IncomeDataType[], type:TypeReqType, i = 0) => {
  if (vehicle.length < i + 1) return
  try {
    const res = await libFetch.postData(`${iwkbuApi}/${type}`, vehicle[i])
    console.log(res.message)
  } catch (err:any) {
    console.log(err.message | err)
  }
  await sendData(vehicle, type, i + 1)
}

const seekData = async (page:Page, codes:string[], month:Dayjs,  reqType:TypeReqType) => {
  // Scrap the IWKBU
  const vehicleIncomeData = await scrapIwkbu(page, codes[0], month,   reqType)
  await sendData(vehicleIncomeData, reqType)
  await page.waitForSelector('#listContainer > ul > li:first-child > a')
  await page.click('#listContainer > ul > li:first-child > a')
  codes.shift()
  if (codes.length < 1) return
  await seekData(page, codes, month, reqType) 
}

const processSwdkllj = async (page:Page, reqType:TypeReqType) => {
  // Scrap the SWDKLLJ
  const menuTeknikSelector = '#listContainer > ul > li:nth-child(3)'
  const menuSwSelector = `${menuTeknikSelector} > ul > li:nth-child(3)`
  const menuManajemenKendaraan = `${menuSwSelector} > ul > li:nth-child(2)`
  await page.waitForNetworkIdle({timeout: (60*60*1000)})
  await page.waitForSelector(menuTeknikSelector)
  await page.click(menuTeknikSelector)
  await page.waitForSelector(menuSwSelector)
  await page.hover(menuSwSelector)
  await page.waitForSelector(menuManajemenKendaraan)
  await page.click(menuManajemenKendaraan)
  await scrapSwdkllj(page, vehicleDasiData, reqType)
  return
}

export default {
  async scraper (dasiReq:DasiReqType) {
    const currentDate = dayjs(dasiReq.date)
    console.log(currentDate)
    console.log('env type = ', process.env.ENV)
    if (dasiReq.codes.length < 1) return console.log('No code provided')
    const browser = await puppeteer.launch({headless:'new'})
    const page = await browser.newPage()
    try{
      await page.goto('https://dasi.jasaraharja.co.id/', {timeout: 60*60*1000})
      await page.setViewport({width: 1080, height: 1024})
      await page.waitForNetworkIdle({timeout: (60*60*1000)})
      const captchaSelector = await page.waitForSelector('input[name="hdCa"]')
      const captcha = await captchaSelector?.evaluate(el => el.value)
      await page.type('input[name="txtUSRID"]', dasiReq.username)
      await page.type('input[name="txtPWD"]', dasiReq.password)
      await page.type('input[name="txtCaptcha"]', captcha as string)
      await page.click('input[name="chkView"]')
      await page.click('input[id="btnPro"]')
      await seekData(page, dasiReq.codes, currentDate, dasiReq.reqType)
      if (vehicleDasiData.length > 0) await processSwdkllj(page, dasiReq.reqType)
    } catch (err) {
      console.log(err)
    } finally {
      console.log("BROWSER_CLOSED")
      await browser.close()
    }
  }
}