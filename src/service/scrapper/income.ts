import puppeteer, { Page } from 'puppeteer'
import libFetch from '@src/library/fetch'
import dayjs from 'dayjs'
import customParseFormat from 'dayjs/plugin/customParseFormat'
import utc from 'dayjs/plugin/utc'

interface DasiReqType {
  user: string,
  pass: string,
  vehicles: string[]
}
interface IncomeDataType {
  nopol: string,
  kantor: string,
  lastReceipt: string,
  lastRecorded: string,
  lastIwkbu: string,
  lastSwdkllj: string,
  lastPrice: number,
  currentReceipt: string,
  currentRecorded: string,
  currentIwkbu: string,
  currentSwdkllj: string,
  currentPrice: number,
}

dayjs.extend(customParseFormat)
dayjs.extend(utc)

const vehicleIncomeData:IncomeDataType[] = []

const scrapSwdkllj = async (page:Page, vehicleData:IncomeDataType[]) => {
  await page.waitForNetworkIdle({timeout: (60*60*1000)})
  try {
    await page.evaluate(() => (document.querySelector('input[name="ctl00$MainContent$txtCariNoPolisi"]') as HTMLInputElement).value = '')
    await page.type('input[name="ctl00$MainContent$txtCariNoPolisi"]', vehicleData[0].nopol)
    await page.click('#ctl00_MainContent_btn_cari')
    await page.waitForNetworkIdle({timeout: (60*60*1000)})
    await page.waitForSelector('#ctl00_MainContent_gridTransaksi > tbody > tr:nth-child(2)', {timeout: 5000})
    const entrySwdkllj = await page.$eval('#ctl00_MainContent_gridTransaksi > tbody', (el) => el.innerHTML)
    const trRegex = /<tr[^>]*>([\s\S]*?)<\/tr>/g
    const tdRegex = /<td[^>]*>([\s\S]*?)<\/td>/g
    let trMatch
    while ((trMatch = trRegex.exec(entrySwdkllj)) !== null) {
      const tdContents = []
      let tdMatch
      while ((tdMatch = tdRegex.exec(trMatch[1])) !== null) {
        const tdText = tdMatch[1].trim()
        if (tdText) tdContents.push(tdText)
      }
      if (tdContents.length < 1) continue
      const takwimYear = tdContents[8].replaceAll(/\D+/g, '')
      if (takwimYear.length < 1) continue
      if (parseInt(takwimYear) < (dayjs().year() - 1)) {
        break
      } else {
        if (parseInt(takwimYear) === dayjs().year()) {
          vehicleData[0].currentSwdkllj = dayjs(tdContents[10], 'DD/MM/YYYY').local().format()
        } else {
          vehicleData[0].lastSwdkllj = dayjs(tdContents[10], 'DD/MM/YYYY').local().format()
        }
      }
    }
  } catch (err) {
    console.log(`Last year SWDKLLJ for ${vehicleData[0].nopol} not found`)
  }
  console.log(vehicleData[0])
  try {
    await libFetch.postData(`http://localhost:3005/webhook/sync-income`, vehicleData[0])
  } catch (err:any) {
    console.log(err.cause)
  }
  vehicleData.shift()
  if (vehicleData.length < 1) return
  await scrapSwdkllj(page, vehicleData)
}

const scrapIwkbu = async (page:Page, vehicleData:IncomeDataType[], index:number, searchYear:number) => {
  const iwkbuData = {
    iwkbu: dayjs('01/01/0001', 'DD/MM/YYYY').local().format(),
    recorded: dayjs('01/01/0001', 'DD/MM/YYYY').local().format(),
    receipt: '',
    price: 0,
  }
  await page.waitForNetworkIdle({timeout: (60*60*1000)})
  try {
    await page.waitForSelector('input[name="ctl00$ContentPlaceHolder1$txt_Tgl_Transaksi_Awal"]')
    await page.evaluate(() => (document.querySelector('input[name="ctl00$ContentPlaceHolder1$txt_Tgl_Transaksi_Awal"]') as HTMLInputElement).value = '')
    await page.evaluate(() => (document.querySelector('input[name="ctl00$ContentPlaceHolder1$txt_Tgl_Transaksi_Akhir"]') as HTMLInputElement).value = '')
    await page.evaluate(() => (document.querySelector('input[name="ctl00$ContentPlaceHolder1$txtKodeKantor"]') as HTMLInputElement).value = '')
    await page.evaluate(() => (document.querySelector('input[name="ctl00$ContentPlaceHolder1$txt_Nopol1"]') as HTMLInputElement).value = '')
    await page.evaluate(() => (document.querySelector('input[name="ctl00$ContentPlaceHolder1$txt_Nopol2"]') as HTMLInputElement).value = '')
    await page.evaluate(() => (document.querySelector('input[name="ctl00$ContentPlaceHolder1$txt_Nopol3"]') as HTMLInputElement).value = '')
    await page.type('input[name="ctl00$ContentPlaceHolder1$txt_Tgl_Transaksi_Awal"]', `01/01/${searchYear}`)
    await page.type('input[name="ctl00$ContentPlaceHolder1$txt_Tgl_Transaksi_Akhir"]', `31/12/${searchYear}`)
    await page.type('input[name="ctl00$ContentPlaceHolder1$txtKodeKantor"]', vehicleData[index].kantor)
    await page.waitForNetworkIdle({timeout: (60*60*1000)})
    await page.waitForSelector('.autocomplete > div')
    await page.click('.autocomplete > div:first-child')
    const nopolSplit = vehicleData[index].nopol.split('-')
    await page.type('input[name="ctl00$ContentPlaceHolder1$txt_Nopol1"]', nopolSplit[0])
    await page.type('input[name="ctl00$ContentPlaceHolder1$txt_Nopol2"]', nopolSplit[1])
    await page.type('input[name="ctl00$ContentPlaceHolder1$txt_Nopol3"]', nopolSplit[2])
    await page.click('#ctl00_ContentPlaceHolder1_btn_Cari')
    await page.waitForNetworkIdle({timeout: (60*60*1000)})
    await page.waitForSelector('#ctl00_ContentPlaceHolder1_gridiwkbu > tbody > tr:nth-child(2)')
    const entryIwkbu = await page.$eval('#ctl00_ContentPlaceHolder1_gridiwkbu > tbody > tr:last-child', (el) => el.innerHTML)
    const labelRegex = /<label[^>]*>([\s\S]*?)<\/label>/g
    const labelContents = []
    let labelMatch
    while ((labelMatch = labelRegex.exec(entryIwkbu)) !== null) {
      labelContents.push(labelMatch[1])
    }
    iwkbuData.recorded = labelContents[2].trim() === '' ? dayjs('01/01/0001', 'DD/MM/YYYY').local().format() : dayjs(labelContents[2].trim(), 'DD/MM/YYYY').local().format()
    iwkbuData.receipt = labelContents[3].trim()
    iwkbuData.iwkbu = labelContents[6].trim() === '' ? dayjs('01/01/0001', 'DD/MM/YYYY').local().format() : dayjs(labelContents[6].trim(), 'DD/MM/YYYY').local().format()
    iwkbuData.price = parseInt(labelContents[8].trim().replaceAll(/\D+/g, ''))
  } catch (err) {
    console.log(`${searchYear} IWKBU for ${vehicleData[index].nopol} not found`)
  }
  if (searchYear === dayjs().year()) {
    vehicleData[index].currentIwkbu = iwkbuData.iwkbu
    vehicleData[index].currentPrice = iwkbuData.price
    vehicleData[index].currentRecorded = iwkbuData.recorded
    vehicleData[index].currentReceipt = iwkbuData.receipt
  } else {
    vehicleData[index].lastIwkbu = iwkbuData.iwkbu
    vehicleData[index].lastPrice = iwkbuData.price
    vehicleData[index].lastRecorded = iwkbuData.recorded
    vehicleData[index].lastReceipt = iwkbuData.receipt
  }
  return
}
const startIwkbu = async (page:Page, vehicleData:IncomeDataType[], index:number) => {
  await scrapIwkbu(page, vehicleData, index, dayjs().year() - 1)
  await scrapIwkbu(page, vehicleData, index, dayjs().year())
  if (index >= vehicleData.length - 1) return
  await page.reload()
  await startIwkbu(page, vehicleData, index + 1)
}

const scrapKantorCode = async (page:Page, vehicleData:string[]) => {
  await page.waitForNetworkIdle({timeout: (60*60*1000)})
  try {
    const nopolSplit = vehicleData[0].split('-')
    await page.evaluate(() => (document.querySelector('input[name="ctl00$ContentPlaceHolder1$txt_nopol_armada"]') as HTMLInputElement).value = '')
    await page.evaluate(() => (document.querySelector('input[name="ctl00$ContentPlaceHolder1$txt_nopol_armada2"]') as HTMLInputElement).value = '')
    await page.evaluate(() => (document.querySelector('input[name="ctl00$ContentPlaceHolder1$txt_nopol_armada3"]') as HTMLInputElement).value = '')
    await page.type('input[name="ctl00$ContentPlaceHolder1$txt_nopol_armada"]', nopolSplit[0])
    await page.type('input[name="ctl00$ContentPlaceHolder1$txt_nopol_armada2"]', nopolSplit[1])
    await page.type('input[name="ctl00$ContentPlaceHolder1$txt_nopol_armada3"]', nopolSplit[2])
    await page.click('#ctl00_ContentPlaceHolder1_btn_cari')
    await page.waitForNetworkIdle({timeout: (60*60*1000)})
    await page.waitForSelector('#ctl00_ContentPlaceHolder1_grid')
    await page.waitForSelector('#ctl00_ContentPlaceHolder1_grid')
    await page.waitForNetworkIdle({timeout: (60*60*1000)})
    await page.waitForSelector('#ctl00_ContentPlaceHolder1_grid')
    await page.waitForNetworkIdle({timeout: (60*60*1000)})
    const entryArmada = await page.$eval('#ctl00_ContentPlaceHolder1_grid > tbody > tr:last-child', (el) => el.innerHTML)
    const labelRegex = /<label[^>]*>([\s\S]*?)<\/label>/g
    const labelContents = []
    let labelMatch
    while ((labelMatch = labelRegex.exec(entryArmada)) !== null) {
      labelContents.push(labelMatch[1])
    }
    const nopol = vehicleData[0]
    const kantor = labelContents[0].trim().replaceAll(/\D+/g, '')
    console.log(`${nopol} = ${kantor}`)
    if (kantor !== '') {
      vehicleIncomeData.push({
        nopol,
        kantor,
        lastReceipt: '',
        lastRecorded: dayjs('01/01/0001', 'DD/MM/YYYY').local().format(),
        lastIwkbu: dayjs('01/01/0001', 'DD/MM/YYYY').local().format(),
        lastPrice: 0,
        lastSwdkllj: dayjs('01/01/0001', 'DD/MM/YYYY').local().format(),
        currentReceipt: '',
        currentRecorded: dayjs('01/01/0001', 'DD/MM/YYYY').local().format(),
        currentIwkbu: dayjs('01/01/0001', 'DD/MM/YYYY').local().format(),
        currentPrice: 0,
        currentSwdkllj: dayjs('01/01/0001', 'DD/MM/YYYY').local().format(),
      })
    }
  } catch (err) {
    console.log(`Kantor for ${vehicleData[0]} not found`)
  }
  vehicleData.shift()
  if (vehicleData.length < 1) return 
  await scrapKantorCode(page, vehicleData)
}

export default {
  async scraper (dasiReq:DasiReqType) {
    const browser = await puppeteer.launch({headless:'new'})
    const page = await browser.newPage()
    await page.goto('https://dasi.jasaraharja.co.id/', {timeout: 60*60*1000})
    await page.setViewport({width: 1080, height: 1024})
    await page.waitForNetworkIdle({timeout: (60*60*1000)})
    const captchaSelector = await page.waitForSelector('input[name="hdCa"]')
    const captcha = await captchaSelector?.evaluate(el => el.value)
    await page.type('input[name="txtUSRID"]', dasiReq.user)
    await page.type('input[name="txtPWD"]', dasiReq.pass)
    await page.type('input[name="txtCaptcha"]', captcha as string)
    await page.click('input[name="chkView"]')
    await page.click('input[id="btnPro"]')

    // Scrap the Kantor Code
    const menuTeknikSelector = '#listContainer > ul > li:nth-child(3)'
    const menuIwSelector = `${menuTeknikSelector} > ul > li:nth-child(2)`
    const menuIwkbuSelector = `${menuIwSelector} > ul > li:nth-child(2)`
    const masterSelector = `${menuIwkbuSelector} > ul > li:first-child`
    const armadaMasterSelector = `${masterSelector} > ul > li:last-child`
    await page.waitForNetworkIdle({timeout: (60*60*1000)})
    await page.waitForSelector(menuTeknikSelector)
    await page.click(menuTeknikSelector)
    await page.waitForSelector(menuIwSelector)
    await page.hover(menuIwSelector)
    await page.waitForSelector(menuIwkbuSelector)
    await page.hover(menuIwkbuSelector)
    await page.waitForSelector(masterSelector)
    await page.hover(masterSelector)
    await page.waitForSelector(armadaMasterSelector)
    await page.click(armadaMasterSelector)
    await scrapKantorCode(page, dasiReq.vehicles)

    // Scrap the IWKBU
    const menu2TeknikSelector = '#listContainer > .art-hmenu > li:nth-child(4)'
    const menu2IwSelector = `${menu2TeknikSelector} > ul > li:nth-child(2)`
    const menu2IwkbuSelector = `${menu2IwSelector} > ul > li:nth-child(2)`
    const operasionalSelector = `${menu2IwkbuSelector} > ul > li:nth-child(3)`
    const penerimaanIwkbuSelector = `${operasionalSelector} > ul > li:nth-child(1)`
    await page.waitForNetworkIdle({timeout: (60*60*1000)})
    await page.waitForSelector(menu2TeknikSelector)
    await page.click(menu2TeknikSelector)
    await page.waitForSelector(menu2IwSelector)
    await page.hover(menu2IwSelector)
    await page.waitForSelector(menu2IwkbuSelector)
    await page.hover(menu2IwkbuSelector)
    await page.waitForSelector(operasionalSelector)
    await page.hover(operasionalSelector)
    await page.waitForSelector(penerimaanIwkbuSelector)
    await page.click(penerimaanIwkbuSelector)
    if (vehicleIncomeData.length > 0) await startIwkbu(page, vehicleIncomeData, 0)

    // Scrap the SWDKLLJ
    const menu2SwSelector = `${menu2TeknikSelector} > ul > li:nth-child(3)`
    const menu2ManajemenKendaraan = `${menu2SwSelector} > ul > li:nth-child(2)`
    await page.waitForNetworkIdle({timeout: (60*60*1000)})
    await page.waitForSelector(menu2TeknikSelector)
    await page.hover(menu2TeknikSelector)
    await page.waitForSelector(menu2SwSelector)
    await page.hover(menu2SwSelector)
    await page.waitForSelector(menu2ManajemenKendaraan)
    await page.click(menu2ManajemenKendaraan)
    if (vehicleIncomeData.length > 0) await scrapSwdkllj(page, vehicleIncomeData)
    
    await browser.close()
  }
}