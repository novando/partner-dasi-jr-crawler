import * as dotenv from 'dotenv'
dotenv.config()

import express from 'express'
import route from '@route/index'

const app = express()
const port = process.env.APP_PORT

app.use(express.json())
app.use('/v1', route)

app.listen(port, () => {
  console.log(`${process.env.APP_NAME} listening on port ${port}`)
})