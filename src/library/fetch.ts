import axios from 'axios'

export default {
  async postData (url:string, payload:object, query = {}, headers = {}) {
    if (query) {
      const queryParam = new URLSearchParams(query)
      url += `?${queryParam.toString()}`
    }
    const res = await axios.post(url, payload, {headers:  Object.assign(headers, {'content-type': 'application/json'})})
    if (res.status !== 200) {
      throw await res.data
    }
    return await res.data
  },
}